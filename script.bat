echo "Criando a imagem"
docker build -t casadodev/projeto-k8s-app-dio:1.0 app/.

echo "Fazendo push da imagen"
docker push casadodev/projeto-k8s-app-dio:1.0

echo "Criando as secrets"
kubectl apply -f ./secrets.yml --record

echo "Fazendo os deployments"
kubectl apply -f ./mysql-deployment.yml --record
kubectl apply -f ./app-deployment.yml --record

echo "Criando os services"
kubectl apply -f ./services.yml --record

echo "Kubernetes configurado para o projeto-k8s-app-dio"
echo ""